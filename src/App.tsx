import React from 'react';
import './App.css';
import { Task } from './modules';

function App() {
  return (
    <div className="App">
     <Task />
    </div>
  );
}

export default App;
