export default class Tasking {
  id: string
  taskName: string
  isCompleted: boolean
  isDeadLine: boolean
  startAt: Date
  deadLine: Date

  constructor({ id, taskName, isCompleted, startAt, deadLine, isDeadLine }: any) {
    this.id = id
    this.taskName = taskName
    this.isCompleted = isCompleted
    this.startAt = startAt
    this.deadLine = deadLine
    this.isDeadLine = isDeadLine
  }
}