import React, { useEffect, useState, useCallback } from 'react'
import InputGroup from 'react-bootstrap/InputGroup';
import Button from 'react-bootstrap/Button';
import FormControl from 'react-bootstrap/FormControl';
import Toast from 'react-bootstrap/Toast';
import { BsTrashFill, BsCircle, BsCheckCircle, BsClockFill } from 'react-icons/bs';
import { FaEdit } from 'react-icons/fa';
import { GiSandsOfTime } from 'react-icons/gi';
import DateTimePicker from 'react-datetime-picker';
import { default as dayjs } from 'dayjs';

import './style.css';
import { useTask } from '../../hooks/useTask';
import { orNull } from '../../utils/Selector';
import Tasking from '../../models/tasking';


function Task() {
  let today = new Date();
  today.setHours(today.getHours() + 1);
  const {
      data, 
      getTaskStorage,
      setTaskStorage,
      removeTaskStorage,
      editTaskStorage,
      completeTaskStorage,
      getUpComingDeadline,
      showAlert,
      toggleShowToast
    } = useTask()
  const [taskValue, setTaskValue] = useState('')
  const [taskId, setTaskId] = useState('');
  const [isEdit, setIsEdit] = useState(false);
  const [deadValue, setDateValue] = useState(today);
  const [isShowClock, setShowClock] = useState(false);

  useEffect(() => {
    getTaskStorage();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    getUpComingDeadline();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])


  const renderUpcomingStatus = (taskItem: Tasking) => {
    if (taskItem.isDeadLine) {
      const deadTime = dayjs(taskItem.deadLine);
      const fromNow = dayjs();
      const remainingTime = deadTime.diff(fromNow, "minutes");
      const deadlineFormat = dayjs(taskItem.deadLine).format('DD/MM/YYYY HH:mm');

      if (remainingTime <= 30) {
        return (
          <div className="itemRowWrapper">
            <GiSandsOfTime className="bloodNeed" />
            <p className="bloodNeedTime">{deadlineFormat}</p> 
          </div>
        );
      }
      return <p className="bloodMana">{deadlineFormat}</p> 
    }
  }

  const renderTaskList = useCallback(() => {
    if (data.length) {
      return data.map((item: Tasking, key) => {
        const taskItemStyle = {
          textDecoration: item.isCompleted ? 'line-through' : 'none'
        }
        return (
          <div key={key} className="itemWrapper">
            <div className="itemRowWrapper">
              {
                item.isCompleted ? <BsCheckCircle className="checked" /> : <BsCircle onClick={() => {
                  completeTaskStorage(item.id, item.isCompleted)
                }} />
              }
              <p className="taskItem" style={taskItemStyle}>{orNull("taskName", item)}</p>
            </div>

            <div className="itemRowWrapper">
              <BsTrashFill className="trash" onClick={() => {
                removeTaskStorage(item.id);
                setTaskValue('');
                setIsEdit(false);
              }} />
              <FaEdit
                className="checked"
                onClick={() => {
                  setIsEdit(true);
                  setTaskValue(item.taskName);
                  setTaskId(item.id);
                }}
              />
              <BsClockFill
                className="clock"
                onClick={() => {
                  setShowClock(!isShowClock);
                  setTaskId(item.id);
                }}
              />
              {renderUpcomingStatus(item)}
            </div>
          </div>
        );
      });
    }
    return <h4>Be busy, don't be lazy. Add some task to Dooo!</h4>

  }, [completeTaskStorage, data, isShowClock, removeTaskStorage]);


  function processDoo() {
    if (!isEdit) {
      setTaskStorage(taskValue);
      setTaskValue('');
    } else {
      editTaskStorage(taskValue, taskId);
      setTaskValue('');
    }
  }


  const renderDeadlineClock = () => {
    if (isShowClock) {
      return (
        <div className="deadlineTimeWrapper">
          <h4>Set Deadline (optional)</h4>
          <DateTimePicker
            onChange={setDateValue}
            value={deadValue}
          />
          <Button
            className="setDeadlineBtn"
            variant="outline-secondary"
            onClick={() => editTaskStorage(taskValue, taskId, deadValue)}
          >Set</Button>
        </div>
      );
    }
  }

  const renderInput = () => {
    return (
      <div className="inputWrapper">
        <InputGroup >
          <FormControl
            placeholder="I want to doo"
            aria-label="I want to doo"
            aria-describedby="basic-addon2"
            onChange={e => setTaskValue(e.target.value)}
            value={taskValue}
          />
          <InputGroup.Append>
            <Button
              disabled={!taskValue.length}
              variant="outline-secondary"
              onClick={() => processDoo()}
            >{isEdit ? 'Edit' : 'Add'}</Button>
          </InputGroup.Append>
        </InputGroup>
      </div>
    );
  }


  const renderToast = () => {
    return (
      <div className="toastWrapper">
        <Toast show={showAlert} onClose={toggleShowToast}>
          <Toast.Header>
            <img src="holder.js/20x20?text=%20" className="rounded mr-2" alt="" />
            <strong className="mr-auto">Upcoming Dooo</strong>
          </Toast.Header>
          <Toast.Body>Baby Shark Doo doo doo!!! Hurry!!</Toast.Body>
        </Toast>
      </div>
    );
  }


  return (
    <div className="container">

      <h1>TWO DOO BABY SHARK</h1>
      {renderTaskList()}
      {renderInput()}
      {renderDeadlineClock()}
      {renderToast()}
    </div>
  );
}

export default Task;
