function set(key: string, data: any) {
  const jsonData = JSON.stringify(data)
  localStorage.setItem(key, jsonData)
}

function get(key: string) {
  const jsonData = localStorage.getItem(key)
  return JSON.parse(jsonData || '[]') 
}

function remove(key: string) {
  localStorage.removeItem(key)
}


function clear() {
  localStorage.clear()
}


export default {
  set,
  get,
  remove,
  clear
}