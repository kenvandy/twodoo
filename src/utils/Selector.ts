import * as R from 'ramda';

const xSplit = (properties: any) => {
  return properties.split('.');
}

export const orArray = (properties: any, record: any): Array<any> => {
  return R.pathOr([], xSplit(properties))(record);
}

export const orNull = (properties: any, record: any): null => {
  return R.pathOr(null, xSplit(properties))(record);
}

export const orObject = (prototypes: any, record: any): object => {
  return R.pathOr({}, xSplit(prototypes))(record);
}

export const orNumber = (prototypes: any, record: any): Number => {
  return R.pathOr(0, xSplit(prototypes))(record);
}
