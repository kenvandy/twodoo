const storageKey = {
  task: {
    TASK_STORAGE_KEY: 'task:taskStorageKey'
  }
}

export default {
  storageKey
}
