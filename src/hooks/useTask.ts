import React, { useState, useCallback } from 'react';
import { default as dayjs } from 'dayjs';

import Tasking from '../models/tasking';
import { Constants, Storage } from '../utils';
import { uniqueIdGenerator } from '../utils/Helpers';


export const useTask = () => {
  const [data, setData] = useState([]);
  const [showAlert, setShowAlert] = useState(false);

  const toggleShowToast = () => setShowAlert(!showAlert);

  const getTaskStorage = useCallback(
    async () => {
      try {
        const taskStorage = await Storage.get(
          Constants.storageKey.task.TASK_STORAGE_KEY
        );
        if (taskStorage && taskStorage.length) {
          setData(taskStorage);
        }
      } catch (error) {
        console.log('GET TASK ERROR', error);
      }
    },
    [],
  )


  const setTaskStorage = useCallback(
    async (taskName: string) => {
      const uniqueID = uniqueIdGenerator();
      let dataArr = [];
      dataArr.push({
        id: uniqueID,
        taskName,
        isCompleted: false,
        startAt: Date.now()
      });
      const taskStorage = await Storage.get(
        Constants.storageKey.task.TASK_STORAGE_KEY
      );
      const newData = taskStorage.concat(dataArr)
      dataArr = []
      Storage.set(
        Constants.storageKey.task.TASK_STORAGE_KEY,
        newData
      )
      getTaskStorage();
    },
    [getTaskStorage],
  )

  const removeTaskStorage = useCallback(
    async (taskId: string) => {
      const taskStorage = await Storage.get(
        Constants.storageKey.task.TASK_STORAGE_KEY
      );
      const itemFilter = taskStorage.filter((item: Tasking) => item.id !== taskId)
      Storage.set(
        Constants.storageKey.task.TASK_STORAGE_KEY,
        itemFilter
      )
      setData(itemFilter)
    },
    [],
  )


  const editTaskStorage = useCallback(
    async (taskName: string, taskId: string, deadLine?: Date) => {
      const taskStorage = await Storage.get(
        Constants.storageKey.task.TASK_STORAGE_KEY
      );
      if (deadLine) {
        const editedTask = taskStorage.map((task: Tasking) => {
          if (task.id === taskId) {
            task.isDeadLine = true;
            task.deadLine = deadLine
          }
          return task
        });

        Storage.set(
          Constants.storageKey.task.TASK_STORAGE_KEY,
          editedTask
        );
        setData(editedTask);

      } else {

        const editedTask = taskStorage.map((task: Tasking) => {
          if (task.id === taskId) {
            task.taskName = taskName;
          }
          return task
        });
        Storage.set(
          Constants.storageKey.task.TASK_STORAGE_KEY,
          editedTask
        );
        setData(editedTask);
      }
    },
    [],
  )


  const completeTaskStorage = useCallback(
    async (taskId: string, isCompleted: boolean) => {
      const taskStorage = await Storage.get(
        Constants.storageKey.task.TASK_STORAGE_KEY
      );
      const editedTask = taskStorage.map((task: Tasking) => {
        if (task.id === taskId) {
          task.isCompleted = !isCompleted;
        }
        return task
      })
      Storage.set(
        Constants.storageKey.task.TASK_STORAGE_KEY,
        editedTask
      );
      setData(editedTask);
    },
    [],
  )


  const getUpComingDeadline = useCallback(
    async () => {
      const taskStorage = await Storage.get(
        Constants.storageKey.task.TASK_STORAGE_KEY
      );
      
      if (taskStorage && taskStorage.length) {
        taskStorage.map((task: Tasking) => {
          if (task.isDeadLine) {
            const deadTime = dayjs(task.deadLine);
            const fromNow = dayjs();
            const remainingTime = deadTime.diff(fromNow, "minutes");

            if (remainingTime <= 30) {
              setShowAlert(true)
            }
          }
          return showAlert
        })
      }
    },
    [showAlert],
  );

  return {
    data,
    setData,
    getTaskStorage,
    setTaskStorage,
    removeTaskStorage,
    editTaskStorage,
    completeTaskStorage,
    getUpComingDeadline,
    showAlert,
    setShowAlert,
    toggleShowToast
  }
}